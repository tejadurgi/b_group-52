# taking current working directory
cwd <- getwd()

# setting current working directory 
setwd(cwd)

# loading data set
df <- read.csv(file = 'Employee_Compensation_SF.csv')


# taking subset of data
empComp = df[sample(nrow(df), 50000), ]

# checking top rows
head(empComp)

# taking only column names
cols = colnames(empComp)


# separating x and y variables
x <- subset(empComp, select = -Total.Compensation)
y <- empComp["Total.Compensation"]


# checking datatypes
str(empComp)


# continuous columns
xint_cols <- c("Salaries", "Overtime", "Other.Salaries", "Total.Salary", 
               "Retirement", "Health.Dental", "Other.Benefits", "Total.Benefits")



#-----------------------scatter plot---------------------
pdf("visualization.pdf") #  Give the chart file a name.

options(scipen=999)

# looping column codes and abbreviation names
for (col in xint_cols){
  
  # scatter plot between two continuous value variables
  plot(empComp[[col]], y[[1]], 
       main = paste({col}, 'VS Total.Compensation'),
       xlab = paste({col}), 
       ylab = "Total.Compensation",
       pch = 20, frame = FALSE)
  
  # plotting regression line between observations
  abline(lm(y[[1]] ~ empComp[[col]], data = empComp), col = "blue")
  
}

#-----------------------box plot-----------------------
par(mfrow=c(1,4)) # setting number of figures in one page

# iterating continuous columns and ploting boxplot for checking each variable distribution 
for (col in xint_cols){
  boxplot(empComp[[col]], main=col, type="l")
  
}


#----------box plot with categorical and continuous variable--------------------------
par(mfrow=c(1,1))

# ploting boxplot for checking annual compensation year wise
boxplot(Total.Compensation ~ Year, data = empComp,
        main = "Annual compensations",
        xlab = "Year",
        ylab = "Total Compensation",
        col = 'bisque')




#---------------------- histogram--------------------------
par(mfrow=c(1,1))

# ploting histogram for dependent variable(continuous variable)
h <- hist(y[[1]], breaks = 10, density = 10,
          col = "lightgray", xlab = "Total.Compensation", main = "Histogram of Total.Compensation") 

xfit <- seq(min(y[[1]]), max(y[[1]]), length = 40) 
yfit <- dnorm(xfit, mean = mean(y[[1]]), sd = sd(y[[1]])) 
yfit <- yfit * diff(h$mids[1:2]) * length(y[[1]]) 

lines(xfit, yfit, col = "black", lwd = 2)


# saving pdf file
dev.off() 




